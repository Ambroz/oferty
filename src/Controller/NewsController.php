<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="news")
     * @return Response
     */
    public function index()
    {
        //pokazuje wszystkie wiadomości
//        $post = new News();
//        $name = $request->get('nameofarticle', $post);
        return $this->render('news/news.html.twig'//,
//            [
//            'name' => $name
//        ]
        );
    }

    /**
     * @Route("/news/show/{nameofarticle}", name="show")
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
//        pokazuje wybraną wiadomośc ze slidera i nie tylko
        $post = new News();
        $name = $request->get('nameofarticle', $post);
        return $this->render('news/show.html.twig', [
            'name' => $name
        ]);
    }

    /**
     * @Route("/news/create", name="create_news")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function create(Request $request)
    {
        //tworzy nową wiadomość

//        $post = new News();
//        $post->setCreateDate(new \DateTime('today', null));
//        $new_news = $this->createForm(NewsType::class, $post);
//
//        $new_news->handleRequest($request);
//
//        if($new_news->isSubmitted()){
//            dump($post);
//        }

        $request->request->get('formElementName');

        $title = $request->request->get('title');
        $content = $request->request->get('content');

        $date = date_create('d-m-Y');

        $createDate = new DateTime($date);

        $strip = $createDate->format('d-m-Y');
//        var_dump($createDate);
//        var_dump($title);

//        dump($title, $content);
        $send = '';
        if (($request->request->get('submit_news'))!==null) {


            $em = $this->getDoctrine()->getManager();
            $news = new News();
            $news->setTitle($title);
            $news->setTextOfNews($content);
            $news->setCreateDate(new \DateTime($strip));
            dump($news);
            $this->addFlash('success', 'Artykół został dodany pomyślnie');
        }
//        echo $title."<br>";
//        echo $content;

        return $this->render('news/create_news.html.twig', [
            'alert' => $send
        ]
    );
    }
}
