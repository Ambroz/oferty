<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder){

        $register_form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'required' => 'true',
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Email'
                ]
            ])
            ->add('nick_name', TextType::class, [
                'required' => 'true',
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'User Name'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'required' => true,
                'first_options'  => ['label' => 'Password', 'attr'=> [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Password'
                ]],
                'second_options' => ['label' => 'Confirm Password', 'attr'=> [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Confirm Password'
                ]],
            ])
            ->add('first_name', TextType::class, [
                'required' => 'true',
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Imie'
                ]
            ])
            ->add('last_name', TextType::class, [
                'required' => 'true',
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Nazwisko'
                ]
            ])
            ->add('user_group_id', HiddenType::class, [
                'attr' => [
                    'value' => '2'
                ]
            ])
            ->add('is_active', HiddenType::class, [
                'attr' => [
                    'value' => '0'
                ]
            ])
            ->add('amount_of_strikes', HiddenType::class, [
                'attr' => [
                    'value' => '0'
                ]
            ])
            ->add('Zarejestruj', SubmitType::class, [
                'attr' => [
                    'class' => 'uk-button uk-button-primary']
        ])
            ->getForm();

        $register_form->handleRequest($request);

        if ($register_form->isSubmitted()){

            $data = $register_form->getData();
            $user = new User();

            $user->setEmail($data['email']);
            $user->setNickName($data['nick_name']);
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $data['password'])
            );
            $user->setFirstName($data['first_name']);
            $user->setLastName($data['last_name']);
            $user->setUserGroupId($data['user_group_id']);
            $user->setIsActive($data['is_active']);
            $user->setAmountOfStrikes($data['amount_of_strikes']);

//            dump($user);die;
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Użytkownik został dodany pomyślnie. Sprawdż Email w celu potwierdzenia rejesttacji!');

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('register/register.html.twig', [
            'register_form' => $register_form->createView()
        ]);
    }
}
